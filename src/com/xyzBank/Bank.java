package com.xyzBank;

import java.util.Date;

public class Bank {
	public static void main(String args[])
	{
		Date date=new Date(2021,1,28);
		Address add=new Address("Hyd","Telangana",500086);
		Savings s1=new Savings("Abhinav",add, 8000,date,"Active");
		s1.deposit(10000);
		System.out.println(s1.withdraw(3000));
		String s= s1.getAddress().getCity()+" "+s1.getAddress().getState()+" "+s1.getAddress().getPin();
		System.out.println(s1.getOwnerName()+" \n"+s+" \n" +s1.getBalAmt() + " \n" + s1.getDate()+" \n"+s1.getStatus());
		System.out.println("Current Account Details");
		Current s2=new Current("Abhinav",add, 8000,date,"Active");
		s2.deposit(10000);
		System.out.println(s2.withdraw(3000));
		String str= s2.getAddress().getCity()+" "+s2.getAddress().getState()+" "+s2.getAddress().getPin();
		System.out.println(s2.getOwnerName()+" \n"+str+" \n" +s2.getBalAmt() + " \n" + s2.getDate()+" \n"+s2.getStatus());
	}
	
	

}
