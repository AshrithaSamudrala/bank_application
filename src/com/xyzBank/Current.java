package com.xyzBank;

import java.util.Date;

public class Current extends Account {
	public Current(String ownerName, Address address, double balAmt, Date date, String status)
	{
		super(ownerName, address, balAmt, date, status);
	}
	double amount=5000;
	public void deposit(double amt)
	{
		balAmt+=amt;
	}
	public String withdraw(double amt)
	{
		if(balAmt-amt<amount)
		{
			return "Insuffient Balance";
		}
		else
		{
			balAmt-=amt;
		}
		return "Balance amount is : "+ balAmt;
		}
}
