package com.xyzBank;

import java.util.Date;

public class Account {
 private String ownerName;
 private Address address; 
 protected double balAmt;
 private Date date;
 private String status;
 

public Account(String ownerName, Address address, double balAmt, Date date, String status) {
	super();
	this.ownerName = ownerName;
	this.address = address;
	this.balAmt = balAmt;
	this.date = date;
	this.status = status;
}
public String getOwnerName() {
	return ownerName;
}
public void setOwnerName(String ownerName) {
	this.ownerName = ownerName;
}
public Address getAddress() {
	return address;
}
public void setAddress(Address address) {
	this.address = address;
}
public double getBalAmt() {
	return balAmt;
}
public void setBalAmt(double balAmt) {
	this.balAmt = balAmt;
}
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
 
}
